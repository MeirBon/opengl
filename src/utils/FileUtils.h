#pragma once

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

namespace utils
{
class FileUtils
{
  public:
	static std::string readFile(const char *filepath)
	{
		std::string buffer;
		std::ifstream file;

		file.exceptions(std::ifstream::failbit | std::ifstream::badbit);

		try
		{
			file.open(filepath);
			std::stringstream fileStream;

			fileStream << file.rdbuf();

			file.close();

			buffer.append(fileStream.str());
		}
		catch (std::ifstream::failure e)
		{
			std::cout << "ERROR::UTILS::FILE_NOT_SUCCESFULLY_READ" << std::endl
					  << "FILE: " << filepath << std::endl;
		}

		return buffer;
	}
};
} // namespace utils
