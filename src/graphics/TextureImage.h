#pragma once

#include <GL/glew.h>
#include <iostream>

#include "stb_image.h"

namespace graphics
{
	class TextureImage
	{
	private:
		GLuint m_TextureId;
		int m_Width;
		int m_Height;
		unsigned int m_Position;
		int m_nrChannels;
		const char *m_File;

	public:
		TextureImage(const char *file, unsigned int position);

		void enable() const;

		void disable() const;

		~TextureImage();

		inline GLuint getTextureId() { return m_TextureId; }
	};
} // namespace graphics