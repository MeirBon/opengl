#include "TextureImage.h"

namespace graphics
{
	TextureImage::TextureImage(const char *file, unsigned int position = 0)
			: m_File(file), m_Position(position)
	{
		// Generate texture
		glGenTextures(1, &m_TextureId);
		glBindTexture(GL_TEXTURE_2D, m_TextureId);

		// Load data
		unsigned char *data =
				stbi_load(m_File, &m_Width, &m_Height, &m_nrChannels, 0);

		if (data)
		{
			// Identify format
			GLenum format;
			if (m_nrChannels == 1)
				format = GL_RED;
			else if (m_nrChannels == 3)
				format = GL_RGB;
			else if (m_nrChannels == 4)
				format = GL_RGBA;

			// Generate mipmap
			glTexImage2D(GL_TEXTURE_2D, 0, format, m_Width, m_Height, 0, format,
						 GL_UNSIGNED_BYTE, data);
			glGenerateMipmap(GL_TEXTURE_2D);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		} else
		{
			std::cout << "Failed to load texture." << std::endl;
		}

		// Free data
		stbi_image_free(data);
	}

	void TextureImage::enable() const
	{

		glActiveTexture(GL_TEXTURE0 + m_Position);
		glBindTexture(GL_TEXTURE_2D, m_TextureId);
	}

	void TextureImage::disable() const
	{ glDisable(GL_TEXTURE_2D); }

	TextureImage::~TextureImage()
	{ glDeleteTextures(1, &m_TextureId); }
} // namespace graphics
