//
// Created by meir on 7/19/18.
//

#include "FontRenderer.h"

namespace graphics
{
	FontRenderer::FontRenderer()
	{
		if (!init())
		{
			std::cout << "Could not init FontRenderer!" << std::endl;
		}
	}

	bool FontRenderer::init()
	{
		int error = FT_Init_FreeType(&m_ft_library);
		return error != 0;
	}

	bool FontRenderer::loadFont(const std::string &fontPath)
	{
		FT_Face font;
		int error = FT_New_Face(m_ft_library, fontPath.c_str(), 0, &font);

		if (error == FT_Err_Unknown_File_Format)
		{
			std::cout << "Font: `" << fontPath << "` appears to have an unsupported format" << std::endl;
			return false;
		}
		else if (error)
		{
			std::cout << "Font: `" << fontPath << "` could not be opened or read" << std::endl;
			return false;
		}

		if (font != nullptr)
		{
			m_Faces[fontPath] = font;
			loadedFonts[fontPath] = true;
			return true;
		}

		return false;
	}

	/**
	 * @param const std::string &fontPath	path to font file, must be loaded before calling setCharSize
	 * @param int char_width	char_width in 1/64th of points
	 * @param int char_height	in 1/64th of points
	 * @param int horizontal_resolution	horizontal device resolution
	 * @param int vertical_resolution	vertical device resolution
	 */
	void FontRenderer::setCharSize(
			const std::string &fontPath,
			int char_width,
			int char_height,
			int horizontal_resolution,
			int vertical_resolution
	)
	{
		if (loadedFonts.find(fontPath) == loadedFonts.end())
		{
			std::cout << "Font not loaded: `" << fontPath << "`" << std::endl;;
			return;
		}

//		int error = FT_Set_Char_Size(
//				m_Faces[fontPath],    /* handle to face object           */
//				0,       /* char_width in 1/64th of points  */
//				16 * 64,   /* char_height in 1/64th of points */
//				300,     /* horizontal device resolution    */
//				300
//		);   /* vertical device resolution      */

		int error = FT_Set_Char_Size(
				m_Faces[fontPath],
				char_width,
				char_height,
				static_cast<FT_UInt>(horizontal_resolution),
				static_cast<FT_UInt>(vertical_resolution)
		);

		if (error)
		{
			std::cout << "Could not set char size of font: `" << fontPath << "`" << std::endl;
		}
	}


	FontRenderer::~FontRenderer() = default;
}