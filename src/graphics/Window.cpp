#include "Window.h"

namespace graphics
{
Window::Window(int width, int height, const char *title,
			   bool fullScreen, Camera *camera)
{
	m_Width = width;
	m_Height = height;
	m_Title = title;
	m_ShouldClose = false;
	m_Camera = camera;
	lastX = m_Width / 2.0f;
	lastY = m_Height / 2.0f;

	if (!init(fullScreen))
	{
		std::cout << "Initialization failed." << std::endl;
		SDL_DestroyWindow(m_Window);
		SDL_Quit();
		exit(-1);
	}
}

bool Window::init(bool fullScreen)
{
	int error = SDL_Init(SDL_INIT_VIDEO);
	if (error < 0)
	{
		std::cout << "Could not initialize SDL!" << std::endl;
		std::cout << "\tSDL Error: " << SDL_GetError() << std::endl;
		return false;
	}

	Uint32 flags = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN;

	if (fullScreen)
	{
		flags = flags | SDL_WINDOW_FULLSCREEN;
	}

	SDL_CaptureMouse(SDL_TRUE);
	SDL_SetRelativeMouseMode(SDL_TRUE);

	m_Window = SDL_CreateWindow(
		m_Title,
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		m_Width,
		m_Height,
		flags);

	if (m_Window == NULL)
	{
		std::cout << "Could not initialize SDL Window!" << std::endl;
		return false;
	}

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);
	SDL_GL_SetAttribute(SDL_GL_FRAMEBUFFER_SRGB_CAPABLE, 1);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

	m_MainContext = SDL_GL_CreateContext(m_Window);

	SDL_GL_MakeCurrent(m_Window, m_MainContext);

	glewExperimental = GL_TRUE;

	GLenum status = glewInit();
	if (status != GLEW_OK)
	{
		std::cout << "Failed to initialize GLEW!" << std::endl;
		std::cerr << "GLEW::ERROR::" << glewGetErrorString(status) << std::endl;
		return false;
	}

	SDL_GL_SetSwapInterval(1);

	return true;
}

bool Window::shouldClose()
{
	return m_ShouldClose;
}

void Window::update() const
{
	GLenum error = glGetError();
	if (error != GL_NO_ERROR)
	{
		std::cout << "OpenGL Error: " << error << std::endl;
		std::cout << "\tDetails: " << glewGetErrorString(error) << std::endl;
	}

	SDL_GL_SwapWindow(m_Window);
}

void Window::pollEvents(float deltaTime)
{
	SDL_Event event;
	SDL_PollEvent(&event);

	if (event.type == SDL_QUIT)
	{
		m_ShouldClose = true;
	}

	if (event.type == SDL_KEYDOWN)
	{
		switch (event.key.keysym.sym)
		{
		case (SDLK_ESCAPE):
			m_ShouldClose = true;
		case (SDLK_w):
			m_Camera->ProcessKeyboard(FORWARD, deltaTime);
		case (SDLK_s):
			m_Camera->ProcessKeyboard(BACKWARD, deltaTime);
		case (SDLK_a):
			m_Camera->ProcessKeyboard(LEFT, deltaTime);
		case (SDLK_d):
			m_Camera->ProcessKeyboard(RIGHT, deltaTime);
		case (SDLK_LCTRL):
			m_Camera->ProcessKeyboard(DOWN, deltaTime);
		case (SDLK_SPACE):
			m_Camera->ProcessKeyboard(UP, deltaTime);
		}
	}

	if (event.type == SDL_WINDOWEVENT_RESIZED)
	{
		glViewport(0, 0, this->getWidth(), m_Height);
	}

	if (event.type == SDL_MOUSEWHEEL)
	{
		m_Camera->ProcessMouseScroll(float(event.motion.yrel));
	}

	if (event.type == SDL_MOUSEMOTION)
	{
		int xPos = event.motion.xrel;
		int yPos = event.motion.yrel;
		if (m_FirstMouse)
		{
			lastX = float(xPos);
			lastY = float(yPos);
			m_FirstMouse = false;
		}

		float xOffset = float(xPos) - lastX;
		float yOffset = lastY - float(yPos); // reversed since y-coordinates go from bottom to top

		lastX = float(xPos);
		lastY = float(yPos);

		m_Camera->ProcessMouseMovement(xOffset, yOffset);
	}
}

void Window::clear() const
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

int Window::getHeight()
{
	SDL_GetWindowSize(m_Window, &m_Width, &m_Height);
	return m_Height;
}

int Window::getWidth()
{
	SDL_GetWindowSize(m_Window, &m_Width, &m_Height);
	return m_Width;
}

Window::~Window()
{
	SDL_GL_DeleteContext(m_MainContext);
	SDL_DestroyWindow(m_Window);
	SDL_Quit();
}
} // namespace graphics
