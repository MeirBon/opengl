#pragma once

#include <iostream>
#include <GL/glew.h>
#include <SDL2/SDL.h>

#include "Camera.h"

namespace graphics
{
	class Window
	{
	private:
		int m_Width, m_Height;
		const char *m_Title;
		SDL_Window *m_Window;
		SDL_GLContext m_MainContext;
		bool m_ShouldClose;
		Camera *m_Camera;
		bool m_FirstMouse = true;
		float lastX, lastY;

		bool init(bool fullScreen);

	public:
		Window(int width, int height, const char *title, bool fullScreen, Camera *camera);

		~Window();

		void clear() const;

		void update() const;

		bool shouldClose();

		void pollEvents(float deltaTime);

		int getWidth();

		int getHeight();
	};
} // namespace graphics
