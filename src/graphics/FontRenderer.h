//
// Created by meir on 7/19/18.
//

#ifndef LEARNGL_FONTREDERER_H
#define LEARNGL_FONTREDERER_H

#include <iostream>
#include <map>
#include <vector>
#include <string>
#include <ft2build.h>

#include FT_FREETYPE_H

namespace graphics
{
class FontRenderer
{
  private:
	FT_Library m_ft_library;
	bool init();
	std::map<std::string, FT_Face> m_Faces;

  public:
	std::map<std::string, bool> loadedFonts;
	bool loadFont(const std::string &fontPath);
	FontRenderer();
	void setCharSize(
		const std::string &fontPath,
		int char_width,
		int char_height,
		int horizontal_resolution,
		int vertical_resolution);

	~FontRenderer();
};
} // namespace graphics

#endif //LEARNGL_FONTREDERER_H
