#include "VertexArray.h"

namespace graphics
{
	namespace buffers
	{
		VertexArray::VertexArray() { glGenVertexArrays(1, &m_ArrayId); }

		VertexArray::~VertexArray()
		{
			for (auto &buff : m_Buffers)
			{
				delete buff;
			}

			glDeleteVertexArrays(1, &m_ArrayId);
		}

		void VertexArray::addBuffer(Buffer *buffer, GLuint index)
		{
			bind();

			buffer->bind();

			glVertexAttribPointer(index, buffer->getComponentCount(), GL_FLOAT, GL_FALSE,
								  0, 0);
			glEnableVertexAttribArray(index);

			buffer->unbind();
			unbind();
		}

		void VertexArray::bind() const { glBindVertexArray(m_ArrayId); }

		void VertexArray::unbind() const { glBindVertexArray(0); }
	} // namespace buffers
} // namespace graphics