#pragma once

#include <GL/glew.h>
#include <vector>

namespace graphics
{
	namespace buffers
	{
		class IndexBuffer
		{
		private:
			GLuint m_BufferId;
			GLuint m_Count;

		public:
			IndexBuffer(GLushort *data, GLsizei count);

			IndexBuffer(GLuint *data, GLsizei count);

			IndexBuffer(std::vector<GLuint> &data);

			~IndexBuffer();

			void bind() const;

			void unbind() const;

			inline GLuint getCount() const { return m_Count; }
		};
	} // namespace buffers
} // namespace graphics