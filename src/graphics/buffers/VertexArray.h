#pragma once

#include <GL/glew.h>
#include <vector>

#include "Buffer.h"

namespace graphics
{
	namespace buffers
	{
		class VertexArray
		{
		private:
			GLuint m_ArrayId;
			std::vector<Buffer *> m_Buffers;

		public:
			VertexArray();

			~VertexArray();

			void addBuffer(Buffer *buffer, GLuint index);

			void bind() const;

			void unbind() const;
		};
	} // namespace buffers
} // namespace graphics