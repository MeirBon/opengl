#pragma once

#include <GL/glew.h>
#include <vector>

namespace graphics
{
	namespace buffers
	{
		class Buffer
		{
		private:
			GLuint m_BufferId;
			GLuint m_ComponentCount;

		public:
			Buffer(GLfloat *data, GLsizei count, GLuint componentCount);

			Buffer(std::vector<GLfloat> &data, GLuint componentCount);

			~Buffer();

			void bind() const;

			void unbind() const;

			inline GLuint getComponentCount() const { return m_ComponentCount; }
		};
	} // namespace buffers
} // namespace graphics