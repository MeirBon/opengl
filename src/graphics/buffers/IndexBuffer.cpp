#include "IndexBuffer.h"

namespace graphics
{
	namespace buffers
	{
		IndexBuffer::IndexBuffer(GLushort *data, GLsizei count) : m_Count(count)
		{
			glGenBuffers(1, &m_BufferId);
			glBindBuffer(GL_ARRAY_BUFFER, m_BufferId);
			glBufferData(GL_ARRAY_BUFFER, count * sizeof(GLushort), data, GL_STATIC_DRAW);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
		}

		IndexBuffer::IndexBuffer(GLuint *data, GLsizei count) : m_Count(count)
		{
			glGenBuffers(1, &m_BufferId);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_BufferId);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(GLuint), data,
						 GL_STATIC_DRAW);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		}

		IndexBuffer::IndexBuffer(std::vector<GLuint> &data)
		{
			glGenBuffers(1, &m_BufferId);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_BufferId);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, data.size() * sizeof(GLuint),
						 &data.front(), GL_STATIC_DRAW);
		}

		void IndexBuffer::bind() const
		{
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_BufferId);
		}

		void IndexBuffer::unbind() const { glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); }

		IndexBuffer::~IndexBuffer() { glDeleteBuffers(1, &m_BufferId); }
	} // namespace buffers
} // namespace graphics
