﻿#include <iostream>
#include <cmath>
#include <chrono>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "graphics/Camera.h"
#include "graphics/Shader.h"
#include "graphics/TextureImage.h"
#include "graphics/Window.h"
#include "graphics/buffers/Buffer.h"
#include "graphics/buffers/IndexBuffer.h"
#include "graphics/buffers/VertexArray.h"
#include "model/Model.h"

using namespace std::chrono;
typedef high_resolution_clock Clock;

int main()
{
	using namespace graphics;
	using namespace buffers;
	using namespace model;

	Clock::time_point lastFrame;
	Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));
	Window window = Window(1920, 1080, "Learn OpenGL", false, &camera);

	Shader shader("shaders/model_loading.vert", "shaders/model_loading.frag");

	Model objModel("models/nanosuit/nanosuit.obj");

	glEnable(GL_DEPTH_TEST);

	// enable shader
	shader.enable();
	lastFrame = Clock::now();

	while (!window.shouldClose())
	{
		// frame-time logic
		Clock::time_point currentFrame = Clock::now();
		duration<float, std::milli> deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		window.pollEvents(deltaTime.count());

		// clear window
		glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		window.clear();

		glm::mat4 projection = glm::perspective(
				glm::radians(camera.Zoom),
				float(window.getWidth()) / float(window.getHeight()),
				0.1f,
				100.0f);
		glm::mat4 view = camera.GetViewMatrix();
		shader.setUniformMatrix4fv("projection", projection);
		shader.setUniformMatrix4fv("view", view);

		glm::mat4 model(1.0f);
		model = glm::translate(model, glm::vec3(0.0f, -1.75f, 0.0f));
		model = glm::scale(model, glm::vec3(0.2f, 0.2f, 0.2f));
		shader.setUniformMatrix4fv("model", model);
		objModel.Draw(shader);

		window.update();
	}

	return 0;
}
