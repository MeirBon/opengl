//
// Created by Mèir Noordermeer on 22/06/2018.
//
#pragma once

#include <iostream>
#include <string>
#include <vector>

#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>

#include "../graphics/TextureImage.h"
#include "Mesh.h"

namespace model
{
	class Model
	{
	public:
		std::vector<Texture> textures_loaded;
		std::vector<Mesh> meshes;
		std::string directory;
		bool gammaCorrection;

		Model(const std::string &path, bool gamma = false);

		void Draw(Shader shader);

	private:
		void loadModel(const std::string &path);

		void processNode(aiNode *node, const aiScene *scene);

		Mesh processMesh(aiMesh *mesh, const aiScene *scene);

		std::vector<Texture> loadMaterialTextures(aiMaterial *mat, aiTextureType type,
												  std::string typeName);

		unsigned int TextureFromFile(const char *path, const std::string &directory,
									 bool gamma);
	};
} // namespace model
