//
// Created by Mèir Noordermeer on 22/06/2018.
//

#pragma once

#include <string>

struct Texture
{
	unsigned int id;
	std::string type;
	std::string path;
};