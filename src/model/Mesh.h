//
// Created by Mèir Noordermeer on 22/06/2018.
//

#pragma once

#include <vector>
#include <string>

#include "../graphics/Shader.h"
#include "../graphics/buffers/Buffer.h"
#include "../graphics/buffers/IndexBuffer.h"
#include "../graphics/buffers/VertexArray.h"
#include "Texture.h"
#include "Vertex.h"

using namespace graphics;
using namespace buffers;

namespace model
{
	class Mesh
	{
	public:
		std::vector<Vertex> vertices;
		std::vector<GLuint> indices;
		std::vector<Texture> textures;
		unsigned int VAO;

		Mesh(std::vector<Vertex> vertices, std::vector<GLuint> indices,
			 std::vector<Texture> textures);

		~Mesh();

		void Draw(Shader shader);

	private:
		unsigned int VBO, EBO;

		void setupMesh();
	};
} // namespace model